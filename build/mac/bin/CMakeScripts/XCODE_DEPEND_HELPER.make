# DO NOT EDIT
# This makefile makes sure all linkable targets are
# up-to-date with anything they link to
default:
	echo "Do not invoke directly"

# For each target create a dummy rule so the target does not have to exist
/usr/lib/libpcre.dylib:
/opt/local/lib/libpcrecpp.dylib:
/usr/lib/libcurl.dylib:
/opt/local/lib/libiconv.2.dylib:
/usr/local/lib/libzip.dylib:
/Users/dokoto/Developments/CPP/LIBS/zlibutils/builds/mac/bin/libzutils.dylib:
/opt/local/lib/libfltk.dylib:


# Rules to remove targets that are older than anything to which they
# link.  This forces Xcode to relink the targets from scratch.  It
# does not seem to check these dependencies itself.
PostBuild.fixItTvShow.Debug:
/Users/dokoto/Developments/CPP/fixtvshow/build/mac/bin/Debug/fixItTvShow:\
	/usr/lib/libpcre.dylib\
	/opt/local/lib/libpcrecpp.dylib\
	/usr/lib/libcurl.dylib\
	/opt/local/lib/libiconv.2.dylib\
	/usr/local/lib/libzip.dylib\
	/Users/dokoto/Developments/CPP/LIBS/zlibutils/builds/mac/bin/libzutils.dylib\
	/opt/local/lib/libfltk.dylib
	/bin/rm -f /Users/dokoto/Developments/CPP/fixtvshow/build/mac/bin/Debug/fixItTvShow


PostBuild.fixItTvShow.Release:
/Users/dokoto/Developments/CPP/fixtvshow/build/mac/bin/Release/fixItTvShow:\
	/usr/lib/libpcre.dylib\
	/opt/local/lib/libpcrecpp.dylib\
	/usr/lib/libcurl.dylib\
	/opt/local/lib/libiconv.2.dylib\
	/usr/local/lib/libzip.dylib\
	/Users/dokoto/Developments/CPP/LIBS/zlibutils/builds/mac/bin/libzutils.dylib\
	/opt/local/lib/libfltk.dylib
	/bin/rm -f /Users/dokoto/Developments/CPP/fixtvshow/build/mac/bin/Release/fixItTvShow


PostBuild.fixItTvShow.MinSizeRel:
/Users/dokoto/Developments/CPP/fixtvshow/build/mac/bin/MinSizeRel/fixItTvShow:\
	/usr/lib/libpcre.dylib\
	/opt/local/lib/libpcrecpp.dylib\
	/usr/lib/libcurl.dylib\
	/opt/local/lib/libiconv.2.dylib\
	/usr/local/lib/libzip.dylib\
	/Users/dokoto/Developments/CPP/LIBS/zlibutils/builds/mac/bin/libzutils.dylib\
	/opt/local/lib/libfltk.dylib
	/bin/rm -f /Users/dokoto/Developments/CPP/fixtvshow/build/mac/bin/MinSizeRel/fixItTvShow


PostBuild.fixItTvShow.RelWithDebInfo:
/Users/dokoto/Developments/CPP/fixtvshow/build/mac/bin/RelWithDebInfo/fixItTvShow:\
	/usr/lib/libpcre.dylib\
	/opt/local/lib/libpcrecpp.dylib\
	/usr/lib/libcurl.dylib\
	/opt/local/lib/libiconv.2.dylib\
	/usr/local/lib/libzip.dylib\
	/Users/dokoto/Developments/CPP/LIBS/zlibutils/builds/mac/bin/libzutils.dylib\
	/opt/local/lib/libfltk.dylib
	/bin/rm -f /Users/dokoto/Developments/CPP/fixtvshow/build/mac/bin/RelWithDebInfo/fixItTvShow


