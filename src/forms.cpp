#include "forms.hpp"
#include <assert.h>
#include <FL/fl_ask.H>

#define CLEAR_DELETE(VAR) if (VAR != NULL) { delete VAR; VAR = NULL; }
	

namespace fix
{

	Forms::Forms(void):
		patron("((\\d?\\d[xX]\\d\\d?)|(\\s\\d?\\d\\.\\d\\d?)|(\\d\\d\\d[-_]))")
	{
		ip_Fichero_carg = NULL;
		ip_Nombre_Serie = NULL;
		ip_Exp_Regular = NULL;
		Grid = NULL;		
		popUp = NULL;
		popUpTable = NULL;
	}

	Forms::~Forms(void)
	{
		CLEAR_DELETE(Grid);
		CLEAR_DELETE(popUpTable);
		CLEAR_DELETE(popUp);
	}


	void Forms::help_Fichero_carga (Fl_Widget*, void* Fthis)
	{
		Forms* fthis = static_cast<Forms*>(Fthis);
		assert(fthis != NULL);

		Fl_File_Chooser chooser(".", "*", Fl_File_Chooser::MULTI, "Seleccionar Fichero de Series...");
		chooser.show();
		while(chooser.shown()) { Fl::wait(); }
		if ( chooser.value() != NULL);
		fthis->ip_Fichero_carg->value(chooser.value());
	}

	void Forms::proc_radios (Fl_Widget*, void* Fthis)
	{
		Forms* fthis = static_cast<Forms*>(Fthis);
		assert(fthis != NULL);

		fthis->TvSelected = fthis->popUpTable->GetSelected();
		fthis->ip_Nombre_Serie->value(fthis->TvSelected.first.c_str());				
		CLEAR_DELETE(fthis->popUpTable);
		CLEAR_DELETE(fthis->popUp);

	}

	void Forms::proc_renombrar_series (Fl_Widget*, void* Fthis)
	{
		Forms* fthis = static_cast<Forms*>(Fthis);
		assert(fthis != NULL);
		std::pair<bool, std::string> Resultado = RenameFiles(fthis->getGrid()->GetvDatas());
		
		if ( Resultado.first == true)
			fl_choice("Fichero Modificados Correctamente.", "Continuar", NULL, NULL);
		else
			fl_choice(Resultado.second.c_str(), "Continuar", NULL, NULL);
	}

	void Forms::proc_series (Fl_Widget*, void* Fthis)
	{
		Forms* fthis = static_cast<Forms*>(Fthis);
		assert(fthis != NULL);

		if (fthis->ip_Fichero_carg->size() == 0)
		{
			fl_choice("La ruta al fichero de Carga es obligatoria para procesarlo.", "Continuar", NULL, NULL);
			return;
		}
		if (fthis->ip_Idioma->size() == 0)
		{
			fl_choice("El codigo de Idioma es Obigatorio para procesar el Fichero.", "Continuar", NULL, NULL);
			return;
		}

		std::vector<SortSeries_t> Resultados;
		std::string DespError;
		if (ProcSeries(fthis->ip_Fichero_carg->value(), fthis->TvSelected.second.c_str(), fthis->TvSelected.first.c_str(), fthis->ip_Idioma->value(),
			fthis->ip_Exp_Regular->value(), Resultados, DespError) == false)
			
			fl_choice(DespError.c_str(), "Continuar", NULL, NULL);
		else
			fthis->Grid->Value(Resultados);
	}

	void Forms::help_Nombre_Serie (Fl_Widget*, void* Fthis)
	{
		Forms* fthis = static_cast<Forms*>(Fthis);
		assert(fthis != NULL);		
		if (fthis->ip_Nombre_Serie->size() == 0)
		{
			fl_choice("El Nombre de la series es obligatorio para lazar la ayuda.", "Continuar", NULL, NULL);
			return;
		}
		if (fthis->ip_Idioma->size() == 0)
		{
			fl_choice("La clave de Idioma es obligatoria para lazar la ayuda.", "Continuar", NULL, NULL);
			return;
		}

		CLEAR_DELETE(fthis->popUpTable);
		CLEAR_DELETE(fthis->popUp);

		std::vector<std::pair<std::string, std::string> > Series;
		CheckNombreSerie(fthis->ip_Nombre_Serie->value(), fthis->ip_Idioma->value(), Series);
		if (Series.empty())
		{
			fl_choice("No se han encontrado series conincidentes. Repita la Busqueda.", "Continuar", NULL, NULL);
		}
		else
		{
			fthis->popUp = new Fl_Double_Window(510, 320, "Listado de Series Conincidentes..");	
			Fl_Button* bt_selected = new Fl_Button(10, 10, 130, 30, "Seleccionar");
			bt_selected->callback(proc_radios, &*fthis);

			fthis->popUpTable = new PopUpSelect(Series, 10, 40, 500, 300);
			fthis->popUp->resizable(fthis->popUp);
			fthis->popUp->end();
			fthis->popUp->show();
		}
	}


	int Forms::RunForms(int argc, char* argv[])
	{
		Fl_Window* MainWindow = new Fl_Window(1024, 680, "Sistema de Correccion de Nombre de Fichero en Series");

		Fl_Box* lb_Fichero_carga = new Fl_Box(10, 10, 150, 30, "Fichero de Carga");
		lb_Fichero_carga->box(FL_UP_BOX);
		lb_Fichero_carga->align(FL_ALIGN_LEFT+FL_ALIGN_INSIDE);

		Fl_Box* lb_Nombre_Serie = new Fl_Box(10, 40, 150, 30, "Nombre Serie");
		lb_Nombre_Serie->box(FL_UP_BOX);
		lb_Nombre_Serie->align(FL_ALIGN_LEFT+FL_ALIGN_INSIDE);

		Fl_Box* lb_Exp_Regular = new Fl_Box(10, 70, 150, 30, "Exp. Regular");
		lb_Exp_Regular->box(FL_UP_BOX);
		lb_Exp_Regular->align(FL_ALIGN_LEFT+FL_ALIGN_INSIDE);
		
		ip_Fichero_carg = new Fl_Input(160, 10, 350, 30);
		ip_Nombre_Serie = new Fl_Input(160, 40, 350, 30);
		ip_Idioma = new Fl_Input(550, 40, 30, 30);
		ip_Idioma->value("es");
		ip_Idioma->maximum_size(2);
		ip_Idioma->color(FL_RED);
		ip_Exp_Regular = new Fl_Input(160, 70, 350, 30);		
		ip_Exp_Regular->value(patron);

		Fl_Button* bt_help_Fichero_carga = new Fl_Button(515, 10, 20, 30, "...");
		bt_help_Fichero_carga->callback(this->help_Fichero_carga, &*this);
		
		Fl_Button* bt_help_Nombre_Serie = new Fl_Button(515, 40, 20, 30, "...");
		bt_help_Nombre_Serie->callback(this->help_Nombre_Serie, &*this);
		
		Fl_Button* bt_proc_series = new Fl_Button(1024-150, 10, 130, 30, "Procesar Series");
		bt_proc_series->callback(this->proc_series, &*this);
		
		Fl_Button* bt_renombrar_series = new Fl_Button(1024-150, 40, 130, 30, "Renombrar Series");
		bt_renombrar_series->callback(this->proc_renombrar_series, &*this);

		std::vector<SortSeries_t> RestVoid;
		//RestVoid.push_back(SortSeries_t(0, 0, "", "", ""));
		Grid = new GridSeries(RestVoid, 10, 120, 1000, 500);

		MainWindow->end();
		MainWindow->show(argc, argv);
		return Fl::run();
	}
}


