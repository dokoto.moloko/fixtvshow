#ifndef FIXIT_PROC_HPP
#define FIXIT_PROC_HPP

#include <string>
#include <vector>

namespace fix
{

	struct SortSeries_t
	{
		size_t Season;
		size_t Episode;
		std::string TitleORG;
		std::string TitleNEW;
		std::string Path;

		SortSeries_t(const size_t season, const size_t episode, const std::string titleORG, const std::string titleNEW, const std::string path) :
			Season(season), Episode(episode), TitleORG(titleORG), TitleNEW(titleNEW), Path(path) {}
		SortSeries_t(const size_t season, const size_t episode, const char* titleORG, const char* titleNEW, const char* path) :
			Season(season), Episode(episode), TitleORG(titleORG), TitleNEW(titleNEW), Path(path) {}
		bool operator < (const SortSeries_t& w) const
		{
			if (Season < w.Season)
				return true;			
			else if (Season == w.Season)			
				return (Episode < w.Episode);
			else return false;
		}
	};

	void CheckNombreSerie(const char* const nombreSerie, const char* const idioma, std::vector<std::pair<std::string, std::string> >& Resultados);
	bool ProcSeries(const char* const path, const char* const idSerie, const char* const Serie, const char* const idioma,
		const char* const patron, std::vector<SortSeries_t>& sortSeries, std::string& DespError);
	std::pair<bool, std::string> RenameFiles(const std::vector<SortSeries_t>& sortSeries);
}

#endif // FIXIT_PROC_HPP

